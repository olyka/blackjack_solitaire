package bj.math;

import static bj.BlackjackSolitaireRunner.*;
import bj.cards.Card;
import bj.cards.Deck;



//   CLUBS("C", "����", "clubs"),
//   DIAMONDS("D", "�����", "diamonds"),
//   HEARTS("H", "������", "hearts"),
//   SPADES("S", "���", "spades");

/**
 * 
 * Test class - alternative for JUnit
 * Just run it in something like Netbeans
 */
public class TestSumPoints {
    
    private final static String[][] testArr = new String[8][];
    private final static int[] testSum = new int[8];
    
    static {
        testArr[0] = new String[16];
        testArr[0][0] = "JS";
        testArr[0][1] = "9H";
        testArr[0][2] = "3C";
        testArr[0][3] = "5D";
        testArr[0][4] = "JC";
        testArr[0][5] = "AH";
        testArr[0][6] = "9C";
        testArr[0][7] = "4C";
        testArr[0][8] = "6H";
        testArr[0][9] = "6C";
        testArr[0][10] = "3D";
        testArr[0][11] = "8S";
        testArr[0][12] = "8C";
        testArr[0][13] = "2D";
        testArr[0][14] = "3H";
        testArr[0][15] = "AS";
        testSum[0] = 24;
    }
    
    static {
	testArr[1] = new String[16];
        testArr[1][0] = "10C";
        testArr[1][1] = "7S";
        testArr[1][2] = "7C";
        testArr[1][3] = "6S";
        testArr[1][4] = "QS";
        testArr[1][5] = "AS";
        testArr[1][6] = "9S";
        testArr[1][7] = "4C";
        testArr[1][8] = "2C";
        testArr[1][9] = "QC";
        testArr[1][10] = "KH";
        testArr[1][11] = "KC";
        testArr[1][12] = "4S";
        testArr[1][13] = "5C";
        testArr[1][14] = "5D";
        testArr[1][15] = "9H";
	testSum[1] = 26;
    }
    
    static {
	testArr[2] = new String[16];
        testArr[2][0] = "KS";
        testArr[2][1] = "9C";
        testArr[2][2] = "8S";
        testArr[2][3] = "6D";
        testArr[2][4] = "AC";
        testArr[2][5] = "AS";
        testArr[2][6] = "4C";
        testArr[2][7] = "9S";
        testArr[2][8] = "6S";
        testArr[2][9] = "JC";
        testArr[2][10] = "6C";
        testArr[2][11] = "4S";
        testArr[2][12] = "QD";
        testArr[2][13] = "8D";
        testArr[2][14] = "9H";
        testArr[2][15] = "3D";
	testSum[2] = 30;
    }
    
    static {
	testArr[3] = new String[16];
        testArr[3][0] = "QH";
        testArr[3][1] = "2C";
        testArr[3][2] = "3H";
        testArr[3][3] = "QS";
        testArr[3][4] = "AC";
        testArr[3][5] = "AD";
        testArr[3][6] = "4H";
        testArr[3][7] = "KH";
        testArr[3][8] = "7C";
        testArr[3][9] = "JH";
        testArr[3][10] = "6D";
        testArr[3][11] = "7H";
        testArr[3][12] = "8S";
        testArr[3][13] = "9D";
        testArr[3][14] = "9S";
        testArr[3][15] = "3D";
	testSum[3] = 41; 
    }
    
    static {
	testArr[4] = new String[16];
        testArr[4][0] = "AS";
        testArr[4][1] = "9H";
        testArr[4][2] = "4D";
        testArr[4][3] = "3S";
        testArr[4][4] = "10D";
        testArr[4][5] = "JD";
        testArr[4][6] = "6S";
        testArr[4][7] = "5H";
        testArr[4][8] = "4H";
        testArr[4][9] = "AC";
        testArr[4][10] = "JH";
        testArr[4][11] = "2D";
        testArr[4][12] = "6D";
        testArr[4][13] = "4C";
        testArr[4][14] = "QH";
        testArr[4][15] = "7D";
	testSum[4] = 42;
    }

    static {
	testArr[5] = new String[16];
        testArr[5][0] = "10S";
        testArr[5][1] = "2D";
        testArr[5][2] = "5S";
        testArr[5][3] = "3S";
        testArr[5][4] = "AC";
        testArr[5][5] = "AD";
        testArr[5][6] = "4D";
        testArr[5][7] = "4C";
        testArr[5][8] = "2H";
        testArr[5][9] = "10C";
        testArr[5][10] = "5C";
        testArr[5][11] = "9C";
        testArr[5][12] = "7C";
        testArr[5][13] = "JD";
        testArr[5][14] = "3H";
        testArr[5][15] = "8D";
	testSum[5] = 67;
    }
    
    static {
	testArr[6] = new String[16];
        testArr[6][0] = "JD";
        testArr[6][1] = "5H";
        testArr[6][2] = "KC";
        testArr[6][3] = "QD";
        testArr[6][4] = "9C";
        testArr[6][5] = "JH";
        testArr[6][6] = "2C";
        testArr[6][7] = "4S";
        testArr[6][8] = "2D";
        testArr[6][9] = "QH";
        testArr[6][10] = "4H";
        testArr[6][11] = "KH";
        testArr[6][12] = "10C";
        testArr[6][13] = "AD";
        testArr[6][14] = "8S";
        testArr[6][15] = "8C";
	testSum[6] = 12;
    }

    static {
	testArr[7] = new String[16];
        testArr[7][0] = "QH";
        testArr[7][1] = "5H";
        testArr[7][2] = "3H";
        testArr[7][3] = "2H";
        testArr[7][4] = "10S";
        testArr[7][5] = "8S";
        testArr[7][6] = "8C";
        testArr[7][7] = "6C";
        testArr[7][8] = "10H";
        testArr[7][9] = "KH";
        testArr[7][10] = "4C";
        testArr[7][11] = "5C";
        testArr[7][12] = "8H";
        testArr[7][13] = "QS";
        testArr[7][14] = "JS";
        testArr[7][15] = "AC";
	testSum[7] = 24;   
    }
    
    
    private static Card[] setTestCardArr(String[] testArr, Card[] cardsArr) {
        
        Card[] cards;
        cards = Deck.setDeckOfCards();
        
        Card cardItem;        
        
        int iterator;
        iterator = 0;
        
        for (String item : testArr) {
            cardItem = Deck.findCardByShortTitle(cards, item);
            if (cardItem != null) {
                cardsArr[iterator] = cardItem;
                iterator++;
            }
        }
        
        iterator = 0;
        cardItem = null;
        
        if (cardsArr.length == MAX_CELL_NUMBER) {
            System.out.println("Cards test array is set!");
        } else {
            System.out.println("Something went wrong with setting the cards test array!");
        }
        
        return cardsArr;
    }
    
    
    public static void main(String[] args) {

        System.out.println("**** Do some testing *******");
        
        int result;
        Card[][] cardsArr = new Card[8][MAX_CELL_NUMBER];
        
        for (int i = 0; i < cardsArr.length; i++) {
            Card[] arr = cardsArr[i];
            
            System.out.println("*****    Run test #" + (i + 1) + "      ******");
            
            arr = setTestCardArr(testArr[i], arr);
            Deck.printDeckOfCards(arr);
            result = PointsSum.getPointsSum(arr);
        
            if (result == testSum[i]) {
                System.out.println("*** Success ***: Summary test for array #" + (i + 1) + " is passed! The sum is " + result + "!");
            } else {
                System.out.println("*** Error ***: Summary test for array #" + (i + 1) + " is failed! The sum is " + result + "!");            
            }
        
            System.out.println("***********");
        }


    }

}
