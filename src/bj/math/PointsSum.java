package bj.math;

import static bj.BlackjackSolitaireRunner.*;
import bj.cards.Card;

/**
 * 
 * Points Summing Class
 */
public class PointsSum {
    
      private final static int[][] COLS_SUM_RULES_ARR = new int[5][];
      static {
        COLS_SUM_RULES_ARR[0] = new int[2];
        COLS_SUM_RULES_ARR[1] = new int[4];
        COLS_SUM_RULES_ARR[2] = new int[4];
        COLS_SUM_RULES_ARR[3] = new int[4];
        COLS_SUM_RULES_ARR[4] = new int[2];
        
        COLS_SUM_RULES_ARR[0][0] = 1;
        COLS_SUM_RULES_ARR[0][1] = 6;
        
        COLS_SUM_RULES_ARR[1][0] = 2;
        COLS_SUM_RULES_ARR[1][1] = 7;
        COLS_SUM_RULES_ARR[1][2] = 11;
        COLS_SUM_RULES_ARR[1][3] = 14;        
        
        COLS_SUM_RULES_ARR[2][0] = 3;
        COLS_SUM_RULES_ARR[2][1] = 8;
        COLS_SUM_RULES_ARR[2][2] = 12;
        COLS_SUM_RULES_ARR[2][3] = 15;        

        COLS_SUM_RULES_ARR[3][0] = 4;
        COLS_SUM_RULES_ARR[3][1] = 9;
        COLS_SUM_RULES_ARR[3][2] = 13;
        COLS_SUM_RULES_ARR[3][3] = 16;        
        
        COLS_SUM_RULES_ARR[4][0] = 5;
        COLS_SUM_RULES_ARR[4][1] = 10;
    }

    private final static int[][] ROWS_SUM_RULES_ARR = new int[4][];
    static {
        ROWS_SUM_RULES_ARR[0] = new int[5];
        ROWS_SUM_RULES_ARR[1] = new int[5];
        ROWS_SUM_RULES_ARR[2] = new int[3];
        ROWS_SUM_RULES_ARR[3] = new int[3];
        
        ROWS_SUM_RULES_ARR[0][0] = 1;
        ROWS_SUM_RULES_ARR[0][1] = 2;
        ROWS_SUM_RULES_ARR[0][2] = 3;
        ROWS_SUM_RULES_ARR[0][3] = 4;        
        ROWS_SUM_RULES_ARR[0][4] = 5;                
        
        ROWS_SUM_RULES_ARR[1][0] = 6;
        ROWS_SUM_RULES_ARR[1][1] = 7;
        ROWS_SUM_RULES_ARR[1][2] = 8;
        ROWS_SUM_RULES_ARR[1][3] = 9;        
        ROWS_SUM_RULES_ARR[1][4] = 10;             

        ROWS_SUM_RULES_ARR[2][0] = 11;
        ROWS_SUM_RULES_ARR[2][1] = 12;
        ROWS_SUM_RULES_ARR[2][2] = 13;
        
        ROWS_SUM_RULES_ARR[3][0] = 14;
        ROWS_SUM_RULES_ARR[3][1] = 15;        
        ROWS_SUM_RULES_ARR[3][2] = 16;       
    }
    
    
    private static int getPointsByRules(int hand, int number) {
        
        int points;
        points = 0;
        
        if ((hand > 0) && (hand <= 16)) {
            points = 1;
        } else {
            switch (hand) {
                case 21:
                    if (number == 2) {
                        points = 10;
                    } else {
                        points = 7;
                    }
                    break;                
                case 20: points = 5;
                    break;
                case 19: points = 4;
                    break;            
                case 18: points = 3;
                    break;            
                case 17: points = 2;
                    break;            
            }            
        }
        return points;
        
    }

    private static int getCardsSum(Card[] arr, int[] sums) {
        
        int result;
        result = 0;
        
        int aceInside;
        aceInside = 0;
        
        String message;

        try {
            for (int i = 0; i < sums.length; i++) {
                if (i <= arr.length) {

                    if (arr[sums[i] - 1].isThisIsAnAce()) aceInside++;
                    result = result + arr[sums[i] - 1].getHand();

                } else {
                    if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {
                        message = "�� ��������� ������� ����� �� �������������� ������!";
                    } else {
                        message = "You try to get card from unexisted cell!";
                    }
                    throw new IndexOutOfBoundsException(); 
                }
            }
        } catch (IndexOutOfBoundsException ioube) {
            System.out.println(ioube.getMessage());
        }
        
        if (aceInside > 0) {
            for (int i = 1; i <= aceInside; i++) {
                if (result >= BUST_NUMBER) {
                    result = result - ACE_NUMBER_TAKE_AWAY;
                }
            }
        }
        
        return getPointsByRules(result, sums.length);
    }
    
    /**
     *
     * Method for scoring in a game
     * @param arr   array of objects of Card class 
     * @return integer 
     */        
    public static int getPointsSum(Card[] arr) {
                
        int result;
        result = 0;
       
        for (int i = 0; i < COLS_SUM_RULES_ARR.length; i++) {
            result = result + getCardsSum(arr, COLS_SUM_RULES_ARR[i]);
        }

        for (int j = 0; j < ROWS_SUM_RULES_ARR.length; j++) {
            result = result + getCardsSum(arr, ROWS_SUM_RULES_ARR[j]);
        }        
        
        return result;

    }
    

       
    
    

}
