package bj;


enum UserLanguages {
    RUS("rus",
        "� ���� BlackjackSolitaire ���������� ������� ����."),   
    ENG("eng", 
        "BlackjackSolitaire game language is set to english.");
   
    private String langArg;
    private String langComment;

    UserLanguages(String langArg, String langComment) {
        this.langArg = langArg;
        this.langComment = langComment;
    }

    public String getArg() {
        return langArg;
    }

    public String getComment() {
        return langComment;
    }
}

/**
 *
 * The main class of the game BlackjackSolitair
  */
public class BlackjackSolitaireRunner {
    
    // �� ��������� ���������� ������� ���� ������� � �������������.
    public final static String DEFAULT_LANGUAGE = "RUS";
    public static String LANGUAGE = DEFAULT_LANGUAGE;
    
    public final static int NUMBER_OF_CARDS_IN_THE_DECK = 52;
    
    public final static int MIN_CELL_NUMBER = 1;
    public final static int MAX_CELL_NUMBER = 16;
    public final static int MIN_TRASH_CELL_NUMBER = 17;
    public final static int MAX_TRASH_CELL_NUMBER = 20;
    
    public final static int BUST_NUMBER = 22;
    public final static int ACE_NUMBER_TAKE_AWAY = 10;
    
    public static void main(String[] args) {
        
        if (((args != null) && (args.length > 0)) 
                && (args[0].equals(UserLanguages.ENG.getArg()))) {
            LANGUAGE = UserLanguages.ENG.name();
            System.out.println(UserLanguages.ENG.getComment());
        } else {
            System.out.println(UserLanguages.RUS.getComment());            
        }
        
        BlackjackSolitaire bjs = new BlackjackSolitaire();
        bjs.play();
        
    }
}

   