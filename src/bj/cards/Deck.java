package bj.cards;

import static bj.BlackjackSolitaireRunner.*;

/**
 * 
 * @author Olyka
 */
public class Deck {

    /**
     * The method for creating a deck of cards.
     * @return Array of objects of Card class
     */
    public static Card[] setDeckOfCards() {
        
        Card[] cards = new Card[NUMBER_OF_CARDS_IN_THE_DECK];

        int i;
        i = 0;
        
        for (Suits suit : Suits.values()) { 
            for (Ranks rank : Ranks.values()) {
                cards[i] = new Card(
                        rank.rank + suit.suit,
                        rank.title + " " + suit.title,
                        rank.titleENG + " " + suit.titleENG,
                        rank.hand,
                        rank.thisIsAnAce);
                i++;
            }
        }
        
        return cards;
    }
    
    /**
     * The method to finding a card by short name (like "AH" etc)
     * @param cards - Array of objects of Card class
     * @param shortTitle - String with card short title 
     * @return Card class object
     */
    public static Card findCardByShortTitle(Card[] cards, String shortTitle) {

        Card card;
        card = null;
        
        for (Card c : cards) {
            if (c.getShortTitle().equals(shortTitle)) {
                card = c;
            }
        }
        
        return card;
    }
 
    /**
     * The method for printing the deck of cards
     * @param cards Array of objects of Card class
     */
    public static void printDeckOfCards(Card[] cards) {
       
        int i = 1;
        for(Card c : cards) {
            if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {
                System.out.print("����� ������");
            } else {
                System.out.print("Cell number");
            }
            System.out.println(" " + i + ". " + c);
            i++;
        }

    }  
    
}
