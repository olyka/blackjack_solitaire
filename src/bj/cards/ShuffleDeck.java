package bj.cards;

/**
 * 
 * Shuffle class
 */
public class ShuffleDeck {
    
    /**
     * Method to get a shuffled deck of cards
     * @return array of objects of Card class 
     */
    public static Card[] setShuffeledDeckOfCards() {
        
        Card[] cards;
        cards = Deck.setDeckOfCards();
        
        Card[] shuffleCards;
        shuffleCards = Randomizer.shuffleArray(cards);
       
        return shuffleCards;
    }

}
