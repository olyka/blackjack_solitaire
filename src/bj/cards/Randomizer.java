package bj.cards;

import java.util.Arrays;
import java.util.Random;

/**
 * 
 * Universal arrays randomizer
 */
public class Randomizer {
    
    /**
     * Method for randomizing arrays of objects of any type
     * @param <T>   element of any type
     * @param defArr     array of objects of any type
     * @return  array of objects of input type
     */
   public static <T> T[] shuffleArray(T[] defArr) {
       
        T[] arr;
        arr = Arrays.copyOf(defArr, defArr.length);
        
        Random randomizer = new Random(); 
        for (int i = 0; i < arr.length; i++) {
            
            int newPosition = randomizer.nextInt(arr.length); 
            T temp = arr[i]; 
            arr[i] = arr[newPosition];  
            arr[newPosition] = temp; 

        }

        return arr;

    }  
  
}


