package bj.cards;

import static bj.BlackjackSolitaireRunner.*;

/**
 *
 * Description for cards suits
 */
enum Suits {
    CLUBS("C", "����", "clubs"),
    DIAMONDS("D", "�����", "diamonds"),
    HEARTS("H", "������", "hearts"),
    SPADES("S", "���", "spades");

    String suit;
    String title;
    String titleENG;
    
    private Suits(String suit, String title, String titleENG) {
        this.suit = suit;
        this.title = title;
        this.titleENG = titleENG;
    }
    
}

/**
 * 
 * Description for cards ranks
 */
enum Ranks {
    TWO("2", 2, "������", "two of", false),
    THREE("3", 3, "������", "three of", false),
    FOUR("4", 4, "��������", "four of", false),
    FIVE("5", 5, "�������", "five of", false),
    SIX("6", 6, "��������", "six of", false),
    SEVEN("7", 7, "�������", "seven of", false),
    EIGHT("8", 8, "���������", "eight of", false),
    NINE("9", 9, "�������", "nine of", false),
    TEN("10", 10, "�������", "ten of", false),
    JACK("J", 10, "�����", "jack of", false),
    QUEEN("Q", 10, "����", "queen of", false),
    KING("K", 10, "������", "king of", false),
    ACE("A", 11, "���", "ace of", true);
    
    String rank;
    int hand;
    String title;
    String titleENG;
    boolean thisIsAnAce;
    
    Ranks(String rank, int hand, String title, String titleENG, boolean thisIsAnAce) {
        this.rank = rank;
        this.hand = hand;
        this.title = title;
        this.titleENG = titleENG;
        this.thisIsAnAce = thisIsAnAce;
    }
    
}

/**
 * 
 * The Card class
 */
public class Card {
    
    public String shortTitle;
    public String title;
    public String titleENG;
    public int hand;        
    public boolean thisIsAnAce;

    /**
     * Constructing a card from parts
     * @param shortTitle    card short title (like "AH" etc)
     * @param title full russian card title
     * @param titleENG  full english card title
     * @param hand  card cost
     * @param thisIsAnAce   if the card is an ace
     */  
    public Card(String shortTitle, String title, String titleENG, int hand, boolean thisIsAnAce) {
        this.shortTitle = shortTitle;
        this.title = title;
        this.titleENG = titleENG;
        this.hand = hand;
        this.thisIsAnAce = thisIsAnAce;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public String getTitle() {
        return title;
    }

    public String getTitleENG() {
        return titleENG;
    }

    public int getHand() {
        return hand;
    }

    public boolean isThisIsAnAce() {
        return thisIsAnAce;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTitleENG(String titleENG) {
        this.titleENG = titleENG;
    }

    public void setHand(int hand) {
        this.hand = hand;
    }

    public void setThisIsAnAce(boolean thisIsAnAce) {
        this.thisIsAnAce = thisIsAnAce;
    }
    
    /**
     * 
     * @return Printing card full name
     */
    @Override
    public String toString() {
        if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {
            return "���� �����: " + shortTitle + " (" + title + ")";
        } else {
            return "Your card: " + shortTitle + " (" + titleENG + ")";            
        }
        
    }
    
}
