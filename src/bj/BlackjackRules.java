package bj;

import static bj.BlackjackSolitaireRunner.*;

/**
 * 
 * Class for printing game rules
 */
public class BlackjackRules {
    
    private static void printGameRulesRUS() {    
        System.out.println("********************************************************************");
        System.out.println("*** ����� ���������� � ���� BLACKJACK SOLITAIRE! ***");
        System.out.println("");
        System.out.println("*** ������� ����: ***");
        System.out.print("����� ���� �� ������ ������� �� ���������� �������������� ������ � �������� � ����� ���� �� 16 ��������� ����� �� �����. ������� ���������� ����� ����������� ������. ���� 4 ��������� ������, ���� ����� ��������� ������� �����, ���� �� �����-�� �������� ��� ������.");
        System.out.println("����� ����� ���������� �������� �� 4 ����. ��� ������ ��� �������� ������ �� ����� ����� ���������, ����� ���� �������������.");
        System.out.println("");
        System.out.println("*** ������� �����: ***");
        System.out.println("����������� ��������� ���-�� ����� � ����� ���� ��� ������� ���� ����� 21. ���� ���������������� ���� � �����-�� ���� ��� ������� �������� ����� 22 � �����, ����� �������� ���������� �������� � ����� ��� ���� � ���� ���� (��� �������) �������.");
        System.out.println("");
        System.out.println("*** �������� ����: ***");
        System.out.print("� ������ ����� ���� �� �������� � �����, ����� ���� �� ������. ���� ����� ��� ��������, �� �������� ������������ ������ �� ������� �������. ������� � ������ ���. ������, ���� � ������ ����������� ���������, �� 10 �����. ");
        System.out.print("���� ������� ����� � ��� ���. ��� �������� ����� ���� ����� 11 ��� 1, � ���������� ��� ���, ����� �������� ��� ����� ������ ����� ��� �������� � ��� ���� � / ��� �������, ��� ��������� ���� ���. ");
        System.out.println("��������, 21 ���� �� ���� ����,  �������� ������ � ������� ������ ��� ������� ����� �������.");
        System.out.println("");        
        System.out.println("--- ���� --- ����� --- �������� ---");
        System.out.println("    ��������    10    ��� ����� � ����� �� ������� ������� ���� � ����� 21 ����");
        System.out.println("    21           7    ���� ��� / ������� ���� 21 ����");
        System.out.println("    20           5    ���� ��� / ������� ���� 20 �����");
        System.out.println("    19           4    ���� ��� / ������� ���� 19 �����");
        System.out.println("    18           3    ���� ��� / ������� ���� 18 �����");
        System.out.println("    17           2    ���� ��� / ������� ���� 17 �����");
        System.out.println("    16 � �����   1    ���� ��� / ������� ���� 16 ����� ��� �����");
        System.out.println("    �������      0    ���� ��� / ������� ���� 22 ���� ��� �����");
        System.out.println("");
        System.out.println("�����!");
        System.out.println("********************************************************************");
        System.out.println("");
    }
    
    private static void printGameRulesENG() {
        System.out.println("********************************************************************");
        System.out.println("*** WELCOME TO BLACKJACK SOLITAIRE GAME! ***");
        System.out.println("");
        System.out.println("*** Goal ***");
        System.out.println("Build multiple Blackjack hands to score 30 points or more.");
        System.out.println("");
        System.out.println("*** The Deal ***");
        System.out.println("The layout starts the game empty. The Stock flips over one card.");
        System.out.println("");
        System.out.println("*** Stock ***");
        System.out.println("The face up card near the Stock must be played to a place in the Blackjack hands or a Discard Pile. As each card is played, the Stock will automatically flip another card.");
        System.out.println("");
        System.out.println("*** Layout ***");
        System.out.println("The 16 spaces may hold one card each. The spaces form nine hands of from 2 to 5 cards each depending on the position of the spaces. There are two hands of 2 cards, two hands of 3 cards, three hands of 4 cards, and two hands of 5 cards.");
        System.out.println("Once a card is placed it may not be moved.");
        System.out.println("");
        System.out.println("*** Discards ***");
        System.out.println("There are four spaces where cards may be placed that are not wanted in the Blackjack hands. Each space may hold one card.");
        System.out.println("");
        System.out.println("*** Scoring ***");
        System.out.println("Scoring for the various Blackjack hands is based on how close the player comes to 21 in each hand. Face cards count as 10. Aces can count as either 1 or 11.");
        System.out.println("The scores from all nine hands are added together to arrive at the final score.");
        System.out.println("Each of the nine hands are scored as follows:");
        System.out.println("--- Hand --- Points --- Example ---");
        System.out.println("    Blackjack    10    A Blackjack is two cards that total 21. A King, Queen, Jack, or 10 combined with an Ace");
        System.out.println("    21           7    3, 4, or 5 cards that total 21");
        System.out.println("    20           5    All cards in the hand total 20");
        System.out.println("    19           4    All cards in the hand total 19");
        System.out.println("    18           3    All cards in the hand total 18");
        System.out.println("    17           2    All cards in the hand total 17");
        System.out.println("    16 and under 1    All cards in the hand total 16 or less");
        System.out.println("    Bust         0    All cards in the hand total 22 or more");
        System.out.println("");
        System.out.println("Good Luck!");
        System.out.println("********************************************************************");
        System.out.println("");
    }
    
    /**
     *
     * The method for printing game rules
     */
    public static void printGameRules() {
    
        if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {
            printGameRulesRUS();
        } else {
            printGameRulesENG();
        }
    
    }    
    
}
