package bj;

import static bj.BlackjackSolitaireRunner.*;
import bj.cards.*;
import bj.io.InputOutput;
import bj.math.PointsSum;

/**
 * 
 * Class for running BlackjackSolitaire game
 */
public class BlackjackSolitaire {
    
    /**
     * The method to run the BlackjackSolitaire game
     */
    public void play() {
        
        BlackjackRules.printGameRules();
        Card[] shuffeledDeck = ShuffleDeck.setShuffeledDeckOfCards();
        // ShuffleDeck.printDeckOfCards(shuffeledDeck);
        
        Card[] cardsPlacedArr;
        cardsPlacedArr = InputOutput.placeCards(shuffeledDeck);
        
        int result;
        result = PointsSum.getPointsSum(cardsPlacedArr);
        
        if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {
            System.out.println("���� ��������!");
            System.out.println("��� ���������: " + result + " �����.");
        } else {
            System.out.println("Game over!");
            System.out.println("Your score: " + result + " points.");
        }
        System.out.println("********************************************************************");
        System.out.println("");
 
    }
    
}
