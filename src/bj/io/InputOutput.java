package bj.io;

import static bj.BlackjackSolitaireRunner.*;
import bj.cards.Card;
import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Stream;


/**
 * 
 * Class for reading and writing game info
 */

public class InputOutput {
    
    private static Scanner sc = new Scanner(System.in);
    
    private static int enterNumber () {
        if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {
            System.out.print("������� ����� ������: ");
        } else {
            System.out.print("Enter cell number: ");
        }        
        String input = sc.nextLine();
        return Integer.parseInt(input);
    }    
    

    private static String printCard(int i, Card[] card) {
        
        String title;
        
        if (i > 0 && i <= card.length) {
            if (card[i - 1] != null) {
                title = card[i - 1].shortTitle;
                if (title.length() == 2) title = " " + title;
            } else {
                if (i < 10) {
                    title = " 0" + i;
                } else {
                    title = " " + i;
                }
            }
        } else {
            title = "";
        }
        
        return title;
    }
    

    private static void printGameField (Card[] arr) {
        System.out.println("-----------------------------------------------------------------------------");
        if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {        
                System.out.println("         ������� ����                                    |        �������     ");
        } else {
                System.out.println("          Game field                                     |         Trash      ");            
        }
        System.out.println("-----------------------------------------------------------------------------");
        System.out.println("  " + printCard(1, arr) + "    " + printCard(2, arr) + "    " + printCard(3, arr) + "    " + printCard(4, arr) + "     " + printCard(5, arr) + "                       |        " + printCard(17, arr) + "   " + printCard(18, arr) + "     ");
        System.out.println("  " + printCard(6, arr) + "    " + printCard(7, arr) + "    " + printCard(8, arr) + "    " + printCard(9, arr) + "     " + printCard(10, arr) + "                       |        " + printCard(19, arr) + "   " + printCard(20, arr) + "     ");
        System.out.println("         " + printCard(11, arr) + "    " + printCard(12, arr) + "    " + printCard(13, arr) + "                               |                    ");
        System.out.println("         " + printCard(14, arr) + "    " + printCard(15, arr) + "    " + printCard(16, arr) + "                               |                    ");
        System.out.println("-----------------------------------------------------------------------------");
    }
    
    /**
     * 
     * Method to get an array of cards located by the player on the playing field
     * @param shuffeledDeck    array of objects of Card class 
     * @return array of objects of Card class 
     */
    public static Card[] placeCards(Card[] shuffeledDeck) {
        
        Card[] fieldArr = new Card[MAX_CELL_NUMBER];
        Card[] trashArr = new Card[MAX_TRASH_CELL_NUMBER - MAX_CELL_NUMBER];
        
        int res;
        res = 0;
        
        int iterator;
        iterator = 1;
        
        String message;

        printGameField(Stream.concat(Arrays.stream(fieldArr), Arrays.stream(trashArr)).toArray(Card[]::new));            
        System.out.println(shuffeledDeck[iterator - 1]);

        while (!Arrays.stream(fieldArr).allMatch(Objects::nonNull)) {
            
            try {

                res = enterNumber();
                
                if (res > 0) {
                    
                    if ((res >= MIN_CELL_NUMBER) && (res <= MAX_CELL_NUMBER)) {
                        
                        if (fieldArr[res - 1] == null) {
                            fieldArr[res - 1] = shuffeledDeck[iterator - 1];
                            iterator++;
                            printGameField(Stream.concat(Arrays.stream(fieldArr), Arrays.stream(trashArr)).toArray(Card[]::new));            
                            System.out.println(shuffeledDeck[iterator - 1]);
                        } else {
                            if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {
                                message = "��� ������ ��� ������, ����������, �������� ������!";
                            } else {
                                message = "This cell is already occupied, please enter another number!";                                
                            }
                            throw new IllegalArgumentException(message);
                        }

                    } else if ((res >= MIN_TRASH_CELL_NUMBER) && (res <= MAX_TRASH_CELL_NUMBER)) {
                        
                        if (!Arrays.stream(trashArr).allMatch(Objects::nonNull)) {
                            if (trashArr[res - MAX_CELL_NUMBER - 1] == null) {
                                trashArr[res - MAX_CELL_NUMBER - 1] = shuffeledDeck[iterator - 1];
                                iterator++;
//                                System.out.println("����� �������� ������: " + res + ". " + trashArr[res - MAX_CELL_NUMBER - 1]);
                                printGameField(Stream.concat(Arrays.stream(fieldArr), Arrays.stream(trashArr)).toArray(Card[]::new));            
                                System.out.println(shuffeledDeck[iterator - 1]);
                            } else {
                                if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {
                                    message = "��� ������ ��� ������ ���� ��� ������, ����������, �������� ������!";
                                } else {
                                    message = "This trash cell is already occupied, please enter another number!";
                                }
                                throw new IllegalArgumentException(message);
                            }
                        } else {
                            if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {
                                message = "������ ��� ������ ���� ��� ������. �� ������ �� ������ ����������� �� ����!";
                            } else {
                                message = "The trash can is full. You can no longer discard cards!";
                            }
                            throw new IllegalArgumentException(message);
                        }
                        
                    } else {
                        if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {
                            message = "����� ������ ����� ���� ������ ������ ��  " + MIN_CELL_NUMBER + " �� " + MAX_TRASH_CELL_NUMBER + "!";
                        } else {
                            message = "The cell number can only be from " + MIN_CELL_NUMBER + " to " + MAX_TRASH_CELL_NUMBER + "!";
                        }
                        throw new IllegalArgumentException(message);
                    }
                } else {
                    if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {
                        message = "����������, ������� ������ ����� ������ ����!";
                    } else {
                        message = "Please, enter only positive numbers!";
                    }
                    throw new IllegalArgumentException(message); 
               }    
                
            } catch (NumberFormatException nfe) {
                if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {
                    message = "����������, ������� ������ ����� � ���� ������ ������!";
                } else {
                    message = "Only numbers can be used to enter the cell number!";
                }                                    
                System.out.println(message);
            } catch (IllegalArgumentException iae) {
                System.out.println(iae.getMessage());                
            } catch (RuntimeException re) {
                System.out.println(re.getMessage());
                break;
            }
        }
        if (LANGUAGE.equals(DEFAULT_LANGUAGE)) {        
            System.out.println("�����������! ��� ����� ����������� �� ������!");
        } else {
            System.out.println("Congratulations! All cards are placed!");
        }
        return fieldArr;

    }
}
