### Консольная версия игры Blackjack Solitaire, созданная мной как выпускной проект для курса Java-разработки SheCodes от компании Luxoft (май 2021):

Console version of Blackjack Solitaire game. 
This is graduation project created by me for Java course SheCodes@Luxoft (May 2021).

https://www.solitairenetwork.com/solitaire/blackjack-square-solitaire-game.html

### Запуск русской версии:

Start russian version of the game

java -jar BlackjackSolitaire.jar 

### Запуск игры с интерфейсом на английском языке:

Start english version of the game

java -jar BlackjackSolitaire.jar eng